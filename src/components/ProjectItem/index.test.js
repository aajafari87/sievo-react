import React from "react";
import { shallow } from "enzyme";
import ProjectItem from "./index";
import moment from "moment";

const data = {
  project: 2,
  description:
    "Substitute Crème fraîche with evaporated milk in ice-cream products",
  "start date": "NULL",
  category: "Office supplies",
  responsible: "Clark Kent",
  "savings amount": 3722.41684,
  currency: "NULL",
  complexity: "Moderate",
};

let wrapped = shallow(<ProjectItem data={data} />);

const expectedValue = value => value === 'NULL' ? '' : value;

describe("Loading", () => {
  it("projectItem category", () => {
    expect(wrapped.find(".category-value").text()).toEqual(expectedValue(data['category']));
  });

  it("projectItem responsible", () => {
    expect(wrapped.find(".responsible-value").text()).toEqual(expectedValue(data['responsible']));
  });

  it("projectItem start date", () => {
    const date = data["start date"] === 'NULL' ? '' : moment(data["start date"]).format("dd.mm.yyyy");
    expect(wrapped.find(".start-date-value").text()).toEqual(date);
  });
  it("projectItem savings amount", () => {
    expect(parseFloat(wrapped.find(".savings-amount-value").text())).toEqual(expectedValue(data["savings amount"]));
  });
  it("projectItem currency", () => {
    expect(wrapped.find(".currency-value").text()).toEqual(expectedValue(data['currency']));
  });
  it("projectItem complexity", () => {
    expect(wrapped.find(".complexity-value").text()).toEqual(expectedValue(data["complexity"]));
  });
});
