import "./assets/css/styles.scss";
import { useEffect, useState } from "react";
import ProjectItem from "./components/ProjectItem";
import { getData } from "./requests/data";
import Loading from "./components/Loading";
import SearchBox from "./components/SearchBox";

function App() {
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [keyword, setKeyword] = useState('');
  const [sort, setSort] = useState(false);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = () => {
    setIsLoading(true);
    getData()
      .then((res) => {
        setIsLoading(false);
        setData(res.data);
      })
      .catch((err) => {
        setIsLoading(false);
        console.log(err);
      });
    return 1;
  };

  const handleOnChangeSearchInput = (keyword) => {
    setKeyword(keyword);
  }

  const showData = (data) => {
    let result = data;
    const filteredData = sortData(data).filter(item => item.description.toLowerCase().trim().search(keyword.toLowerCase().trim()) > -1);
    if (filteredData.length > 0) {
      return filteredData
    }
    return result;
  }

  const sortData = () => {
    let sortableItems = [...data];
    if (sort) {
      sortableItems.sort((a, b) => {
        const dateA = new Date(a['start date']);
        const dateB = new Date(b['start date']);
        return dateB - dateA;
      });
    }
    return sortableItems;
  }

  return (
    <div className="container-fluid">
      <Loading isLoading={isLoading} />
      <div className='header'>
        <div className='search-box'>
          <SearchBox value={keyword} handleOnChange={handleOnChangeSearchInput} />
        </div>
        <span className={`sort-btn ${sort ? 'selected' : ''}`} onClick={() => setSort(!sort)}>Sort by Date</span>
      </div>
      <div className="row">
        {showData(data).map((item, index) => {
          return <ProjectItem key={index} data={item} />;
        })}
      </div>
    </div>
  );
}

export default App;
