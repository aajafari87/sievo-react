import axios from "axios";

export const getData = async () => {
  return await axios.get(
    "https://sievo-react-assignment.azurewebsites.net/api/data"
  );
};
