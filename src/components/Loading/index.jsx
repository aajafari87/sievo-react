import React from "react";
import styles from "./Loading.module.scss";

function Loading({ isLoading }) {
  return (
    isLoading && (
      <div id={"loading-wrapper"} className={styles.loadingWrapper}>
        Loading ...
      </div>
    )
  );
}

export default Loading;
