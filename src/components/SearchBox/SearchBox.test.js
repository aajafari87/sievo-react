import React from "react";
import { shallow, mount } from "enzyme";
import SearchBox from "./index";

// let wrapped = shallow(<SearchBox />);

const wrapper = (props = {}) => {
    return shallow(<SearchBox {...props} />)
}

describe("Search Box", () => {
    it("render searchbox without error", () => {
        expect(wrapper().find(".search-box").length).toEqual(1);
    });
    it('add value prop', () => {
        const input = wrapper({ value: 'Kryptonite' }).find('input');
        expect(input.get(0).props.value).toEqual('Kryptonite');
    });
});
