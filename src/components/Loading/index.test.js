import React from "react";
import { shallow } from "enzyme";
import Loading from "./index";
let wrapped = shallow(<Loading isLoading={true} />);
describe("Loading", () => {
  it("should render the Loading Component correctly", () => {
    expect(wrapped).toMatchSnapshot();
  });
  it("renders the Loading children", () => {
    expect(wrapped.find("#loading-wrapper").text()).toEqual("Loading ...");
  });
});
