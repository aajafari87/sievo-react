import React from "react";
import moment from "moment";

import styles from "./ProjectItem.module.scss";

function ProjectItem({ data }) {
  return (
    <div className={` ${styles.projectItem}`}>
      <div>
        <ul className={styles.projectItem__detailList}>
          <li>
          <span className={styles.projectItem__detailTitle}>Description:</span>
            <span
              className={`${styles.projectItem__detailValue} description-value`}
            >
              {data.description}
            </span>
          </li>        
          <li>
            <span className={styles.projectItem__detailTitle}>Start Date:</span>
            <span
              className={`${styles.projectItem__detailValue} start-date-value`}
            >
              {data["start date"] !== "NULL" &&
                moment(data["start date"]).format("dd.mm.yyyy")}
            </span>
          </li>
          <li>
            <span className={styles.projectItem__detailTitle}>Category:</span>
            <span
              className={`${styles.projectItem__detailValue} category-value`}
            >
              {data.category}
            </span>
          </li>
          <li>
            <span className={styles.projectItem__detailTitle}>
              Responsible:
            </span>
            <span
              className={`${styles.projectItem__detailValue} responsible-value`}
            >
              {data.responsible}
            </span>
          </li>
          <li>
            <span className={styles.projectItem__detailTitle}>
              Savings Amount:
            </span>
            <span
              className={`${styles.projectItem__detailValue} savings-amount-value`}
            >
              {data["savings amount"]}
            </span>
          </li>

          <li>
            <span className={styles.projectItem__detailTitle}>Currency:</span>
            <span
              className={`${styles.projectItem__detailValue} currency-value`}
            >
              {data.currency === "NULL" ? "" : data.currency}
            </span>
          </li>

          <li>
            <span className={styles.projectItem__detailTitle}>Complexity:</span>
            <span
              className={`${styles.projectItem__detailValue} complexity-value`}
            >
              {data.complexity}
            </span>
          </li>
        </ul>
      </div>
    </div>
  );
}

export default ProjectItem;
